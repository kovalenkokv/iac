##############   FW RULES ####################$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

resource "cloudflare_ruleset" "terraform_managed_resource_0a17887c56b94741b1c23ecb89b40b9d" {
  kind    = "zone"
  name    = "default"
  phase   = "http_request_firewall_custom"
  zone_id = "52192b467297aeeba911c9efc33dac03"
  rules {
    action = "skip"
    action_parameters {
      phases   = ["http_ratelimit", "http_request_firewall_managed", "http_request_sbfm"]
      products = ["zoneLockdown", "bic", "uaBlock", "hot", "securityLevel", "rateLimit", "waf"]
      ruleset  = "current"
    }
    description = "wordpress"
    enabled     = true
    expression  = "(ip.src in {91.245.37.218 193.176.179.254})"
    logging {
      enabled = true
    }
    ref = "11a7c7578c044bddbd857cbfb2d35ec9"
  }
}



#resource "cloudflare_firewall_rule" "terraform_managed_resource_d6e5d106cf174aada3433af6c97c00d8" {
#  action      = "allow"
#  description = "wordpress"
#  filter_id   = "c26291860179460598526e4aa21f0b87"
#  paused      = false
#  zone_id     = "52192b467297aeeba911c9efc33dac03"
#}

############# DNS RECORDS ######################
############# DNS RECORDS ######################
resource "cloudflare_record" "storj_koct9i_ru" {
  name    = "storj"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "gl_koct9i_ru" {
  name    = "gl"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "n8n_koct9i_ru" {
  name    = "n8n"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}
resource "cloudflare_record" "nextcloud_koct9i_ru" {
  name    = "nc"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}
resource "cloudflare_record" "git_koct9i_ru" {
  name    = "git"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}
resource "cloudflare_record" "terraform_managed_resource_821f39477b5df5459ba14c0914568f3c" {
  name    = "geg.internal"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "192.168.18.4"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_fe90e57460dc83828aa57ee051d5963b" {
  name    = "gw.internal"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "192.168.18.1"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_fcb1201e669de41811801577959fc1f9" {
  name    = "host.internal"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_5c5db367d759f2b3370dd8bcc373a944" {
  name    = "koct9i.ru"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_66c1f53b18b7101d521bc8b308df1407" {
  name    = "vpn.internal"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "193.176.179.254"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_ea9b7b6c8c3dda9c3d98480f400eaccf" {
  name    = "www"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_e480ce0691f128f80e15514d20baf238" {
  name    = "kovalenko"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "koct9i.ru"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_1d7ec9152503398af4f60239dd40db68" {
  name    = "mail"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "biz.mail.ru"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_211d54d581987a6926e97ea58566ce10" {
  name     = "koct9i.ru"
  priority = 10
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "emx.mail.ru"
  zone_id  = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_682ee825da70945ecce708ab6a19ff3c" {
  name    = "_dmarc"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=DMARC1;  p=none; rua=mailto:d1d2f97a10a54bd0b4a91f13ab6aa310@dmarc-reports.cloudflare.net"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_7ad307fd21c23187b8e30ca2c5569136" {
  name    = "koct9i.ru"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=spf1 ip4:91.245.37.218 a mx include:_spf.mail.ru ~all"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_52ed5ce0fb45b279f013551a7832de0c" {
  name    = "koct9i.ru"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "mailru-domain: HRGA0k5W5WpAp8jV"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_c34deee40d9a240bc70dd9f51102201d" {
  name    = "mailru._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "\"v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChnTIcXUvEosFGc/zR1KGmAFJJeUkfpH+SdcARzrvPSg9wnIO9bCoIiIuiYXNVz4DlLTqNpdy0d/RfWWPr5tgpb+NTV3NQsoWQQi4tIouPEojh4iEW4wiHIrwmSYAqwofV0Bhu/OCIlkCIFtEdmptsOmiTUrVSJTu22LQAdj7nsQIDAQAB\""
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

resource "cloudflare_record" "terraform_managed_resource_390b323eeb31a3fb76a540c72d51efc4" {
  name    = "mailru._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChnTIcXUvEosFGc/zR1KGmAFJJeUkfpH+SdcARzrvPSg9wnIO9bCoIiIuiYXNVz4DlLTqNpdy0d/RfWWPr5tgpb+NTV3NQsoWQQi4tIouPEojh4iEW4wiHIrwmSYAqwofV0Bhu/OCIlkCIFtEdmptsOmiTUrVSJTu22LQAdj7nsQIDAQAB"
  zone_id = "52192b467297aeeba911c9efc33dac03"
}

