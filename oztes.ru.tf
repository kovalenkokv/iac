




######################
#   DNS records
######################
resource "cloudflare_record" "terraform_managed_resource_7dc10a7a05f77c9a0da12883d4a5b4f4" {
  name    = "oztes.ru"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "91.245.37.218"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_66dd125e8ea7d5409924c5802aa8705e" {
  name    = "host"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "oztes.ru"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_08a81c1d5dde5bb109ddc792257aec17" {
  name    = "nc"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "oztes.ru"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_e4e71d236bb402576a2725f70c16245b" {
  name    = "office"
  proxied = true
  ttl     = 1
  type    = "CNAME"
  value   = "oztes.ru"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_e59df3766bc7d8fe4b5e60bce980e496" {
  name    = "vpn"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "oztes.ru"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_d7ac971cfc3e81a3e538f544ed7cf96e" {
  name    = "www"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "oztes.ru"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_67fdde1584b97ca82387b2eea9acc8c7" {
  name     = "oztes.ru"
  priority = 10
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "emx.mail.ru"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_1a4b68fc16bdd4bc55f98b92d0e889d9" {
  name     = "_autodiscover._tcp"
  priority = 10
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 443
    priority = 10
    proto    = "_tcp"
    service  = "_autodiscover"
    target   = "autodiscover.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_2e34a9cd8cea39e546387f3857233d6d" {
  name     = "_imaps._tcp"
  priority = 0
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 993
    priority = 0
    proto    = "_tcp"
    service  = "_imaps"
    target   = "imap.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_91ae2eb68fa59a88943956d16568394f" {
  name     = "_imap._tcp"
  priority = 5
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 143
    priority = 5
    proto    = "_tcp"
    service  = "_imap"
    target   = "imap.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_c91f5683f877c00cb4573288e12aa71c" {
  name     = "_pop3s._tcp"
  priority = 10
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 995
    priority = 10
    proto    = "_tcp"
    service  = "_pop3s"
    target   = "pop3.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_811ef8c4604e6e96e9e3d1fea7bc5673" {
  name     = "_submissions._tcp"
  priority = 0
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 465
    priority = 0
    proto    = "_tcp"
    service  = "_submissions"
    target   = "smtp.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_7e3ec1322e2945d638ddec1db61a2d35" {
  name     = "_submission._tcp"
  priority = 10
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 587
    priority = 10
    proto    = "_tcp"
    service  = "_submission"
    target   = "smtp.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_2cc1b8d6ca1aa57dc8d9298f86dd0d37" {
  name     = "_submission._tcp"
  priority = 30
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 25
    priority = 30
    proto    = "_tcp"
    service  = "_submission"
    target   = "smtp.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_cca85bcf09268ba0491e1860a9903103" {
  name     = "_submission._tcp"
  priority = 20
  proxied  = false
  ttl      = 1
  type     = "SRV"
  zone_id  = "ce922c70839f4606f10694d71ba1cd3f"
  data {
    name     = "oztes.ru"
    port     = 2525
    priority = 20
    proto    = "_tcp"
    service  = "_submission"
    target   = "smtp.mail.ru"
    weight   = 1
  }
}

resource "cloudflare_record" "terraform_managed_resource_1e1f09f5e710783811ac28e94e9ec3e8" {
  name    = "mailru._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtpDvjKpDtxLIdFz6XZtxsOzkVMbgfRmGKr3UYT2ynWQuAdwdGgRW9M0Pg+9AHr9XrGqvx/yfnNyCCK5vXRo0zvsfWZbXQjnzbT+TqIqex/wjhtr4GYTd8NiDU5u5Z2NWQQ14JH08hQnhhymlczzQE8IEYBNfbkG4MpKhs2++w2QIDAQAB"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_3a042346435ac697bfc6b9ecf144533f" {
  name    = "oztes.ru"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "mailru-domain: V0PvUfxScmA0W0cc"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

resource "cloudflare_record" "terraform_managed_resource_bbe86e87281a10f63adf24ce49e54a86" {
  name    = "oztes.ru"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=spf1 redirect=_spf.mail.ru"
  zone_id = "ce922c70839f4606f10694d71ba1cd3f"
}

