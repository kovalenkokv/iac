terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.0"
    }
  }
  backend "http" { }
}
provider "cloudflare" {
  # Configuration options
}
